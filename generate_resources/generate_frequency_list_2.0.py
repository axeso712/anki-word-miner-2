# -*- coding: utf-8 -*-

import json
import re

#opens the frequency list json file (must be named "frequency_list.json")
with open('frequency_list.json', 'r') as f:
    frequency_list = json.load(f)

#deletes old frequency from the list elements
for i in frequency_list:
    del(i[1],i[1])

#specifies a search for all charcters exept hiragana, katakana, some special symbols and numbers
pattern = r'[^あいうえおかきくけこがぎぐげごさしじすずせそざじずぜぞ \
              たちつてとだぢづでどなにぬねのはひふへほばびぶべぼぱ \
              ぴぷぺぽまみむめもやゆよらりるれろわゐゑをんっょぁゅゃぇ\
              ァアイウエオカキクケコサシスセソタチツテトナニヌネノハヒフヘ\
              ホマミムメモヤユヨラリルレロワンガギグゲゴザジズゼゾダヂヅ\
              ドデバビブベボパピプペポヴゥャッィェォュェョー・゜0123456789２]'

#declare new list
new_frequency_list = []

#goes through entire frequency list
for i in range(len(frequency_list)):
    #does search according to pattern, if there is a match it appends that element to new list
    if re.search(pattern, frequency_list[i][0]):
        new_frequency_list.append(frequency_list[i])

#add frequency numbers to words in new list
for i in range(len(new_frequency_list)):
    new_frequency_list[i].append(i+1)

#save txt file with a word and corresponding frequency from new frequency list on each line
new_frequency = open("new_frequency_list.txt", "w+")
for i in new_frequency_list:
    new_frequency.write(i[0] + "," + str(i[1]) + '\n')
new_frequency.close()

#saves the new frequency list as json file
json_frequency_list = json.dumps(new_frequency_list, ensure_ascii=False)
with open('freq_list_2.0.json', 'w') as json_file:
    json.dump(new_frequency_list, json_file, ensure_ascii=False)  