import json

'''takes json lists in the format of the yomichan importer and 
    turns them into a large dictionary where the words are the keys
    and the definitions are the answers'''

#takes in a number and returns a dictionary file name as a string
def term_bank(number):
    number = str(number)
    dictionary_num = "term_bank_" + number + '.json'
    return dictionary_num

#takes in a nunmber and returbs a dictionary list with that number in its name
def load_dictionary (dictionary_number):
    dic = term_bank(dictionary_number)
    with open(dic, 'r') as f:
        term_list = json.load(f)
    return term_list

#takes in a dictionary list and flattens the second subitem of the list's elements
def flatten_dictionary(dictionary):
    flat_dictionary = []
    for i in dictionary:
        flat_element = [i[0], i[1][0]]
        flat_dictionary.append(flat_element)
    return flat_dictionary

#takes in a dictionary list and formats it to make it easier to turn into a dictionary
def format_dictionary(dictionary):
    for i in dictionary:
        del(i[1], i[1], i[1], i[1])
        del(i[-1], i[-1])
    dictionary = flatten_dictionary(dictionary)
    return dictionary

#takes in a number and returns a formatted dictionary list with that number as its file ending
def get_dictonary(number):
    dictionary = load_dictionary(number)
    formatted_dictionary = format_dictionary(dictionary)
    return formatted_dictionary

#takes in a range of numbers and returns a large list of all the lists with the numbers in their file ending
def create_large_dic_list(startnumber,endnumber):
    large_dictionary_list = []
    for i in range (startnumber,endnumber + 1):
        large_dictionary_list = large_dictionary_list + get_dictonary(i)
    return large_dictionary_list

#takes in a list and turns it into a dictionary
def create_dic_dictionary(dictionary_list):
    dic_dictionary = {}
    for i in dictionary_list:
        dic_dictionary[i[0]] = i[1]
    return dic_dictionary

#delivers the final dictionary
def main():
    large_list = create_large_dic_list(1,23)
    final_dictionary = create_dic_dictionary(large_list)

     #saves the new frequency list as json file
    with open('japanese_def_dictionary.json', 'w') as json_file:
        json.dump(final_dictionary, json_file, ensure_ascii=False) 


if __name__ == "__main__": 
    main()
     