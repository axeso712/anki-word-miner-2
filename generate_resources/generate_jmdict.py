import json
import generate_japanese_def_dictionary 

'''Takes in yomichan-formatted jmdict as a json list and keeps only reading, 
    vocabulary position and translation, for each word. It then puts this information in 
    a separate dictionary for each and asigns a overarching dictionary for all of them. 
    The final product that gets returned is this head dictionary with all information''' 

#loads a dictionary with tag abbrivieations as keys and tags as answers
with open('tag_bank_dictionary.json', 'r') as f:
    tag_dict = json.load(f)   

#takes in a number and returns a dictionary file name as a string
def term_bank(number):
    number = str(number)
    dictionary_num = "term_bank_" + number + '.json'
    return dictionary_num

#takes in a nunmber and returbs a dictionary list with that number in its name
def load_dictionary (dictionary_number):
    dic = term_bank(dictionary_number)
    with open(dic, 'r') as f:
        term_list = json.load(f)
    return term_list

#takes a list element as argument and replaces its abbriveated vocab pos with full one's
def format_vocab_pos(list_element):
    tag_element = list_element.pop(2) 
    tag_element_list = tag_element.split()
    new_tag_element_list = []
    for j in tag_element_list:
        new_tag_element_list.append(tag_dict[j])
    str_new_tag_element_list = ", ".join(new_tag_element_list)
    return str_new_tag_element_list

#takes in a dictionary list and formats it to make it easier to turn into a dictionary
def format_dictionary(dictionary):
    for i in dictionary:

        #deletes unnecessary list elements
        del(i[3], i[3], i[-1], i[-1])

        #format vocab-pos
        i.insert(2, format_vocab_pos(i))

        #change translation type from list to string
        i[3] = ', '.join(i[3])



    #searches for duplicate word entries, if they are found their translation is concatinated
    for i in range(len(dictionary)):
        check = dictionary[i][0]
        for j in range(i+1, len(dictionary)):
            if dictionary[j][0] == check:
                dictionary[i][3] = dictionary[i][3] + '; ' + dictionary[j][3]

    #then the duplicate word entry is removed
    new_dict = [] 
    check_list = []
    for i in dictionary: 
        if not new_dict:
            new_dict.append(i)
            check_list.append(new_dict[0][0])
        else:
            if i[0] not in check_list:
                    new_dict.append(i)
                    check_list.append(i[0])
    return new_dict

#takes in a number and returns a formatted dictionary list with that number as its file ending
def get_dictonary(number):
    dictionary = load_dictionary(number)
    formatted_dictionary = format_dictionary(dictionary)
    return formatted_dictionary

#takes in a range of numbers and returns a large list of all the lists with the numbers in their file ending
def create_large_dic_list(startnumber,endnumber):
    large_dictionary_list = []
    for i in range (startnumber,endnumber + 1):
        large_dictionary_list = large_dictionary_list + get_dictonary(i)
    return large_dictionary_list

#takes the information in the list and coverts it to a dictionary with the japanese words as keys
def make_actual_dictionary(dictionary_list):

    #declaring dictionaries 
    reading_dictionary = {}
    position_dictionary = {}
    translation_dictionary = {}
    jmdict_dictionary = {}
    
    #putting information from the list into their respective dictionaries
    for i in dictionary_list: 
        reading_dictionary[i[0]] = i[1]
        position_dictionary[i[0]] = i[2]
        translation_dictionary[i[0]] = i[3]

    #creating a large dictionary with all the information for easy access
    jmdict_dictionary['reading'] = reading_dictionary
    jmdict_dictionary['vocabulary position'] = position_dictionary
    jmdict_dictionary['translation'] = translation_dictionary
    
    return jmdict_dictionary

if __name__ == "__main__":
    #I know this isn't how you're supposed to do it, but i just need this code one time anyway
    final_dictionary = make_actual_dictionary(create_large_dic_list(1,29))
    with open('jmdict_dictionary.json', 'w') as dictonary_file:
        json.dump(final_dictionary, dictonary_file, ensure_ascii=False)
    


