import json


with open ('jmdict_dictionary.json', 'r') as f:
    jmdct = json.load(f)

with open('japanese_def_dictionary.json', 'r') as f:
    daijisen = json.load(f)

with open ('freq_list_2.0.json', 'r') as f:
    freq = json.load(f)

freq_dict = {}
reverse_freq_dict = {}
for word in freq:
    freq_dict[word[0]] = word[1]
    reverse_freq_dict[str(word[1])] = word[0]    

jmdct['definition'] = daijisen
jmdct['frequency'] = freq_dict
jmdct['reverse frequency'] = reverse_freq_dict

with open('final_dict.json', 'w') as f:
    json.dump(jmdct, f, ensure_ascii=False)