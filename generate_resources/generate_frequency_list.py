# -*- coding: utf-8 -*-

import sqlite3
import os

HIRAGANA = "あいうえおかきくけこがぎぐげごさしすせそざじずぜぞ \
            たちつてとだぢづでどなにぬねのはひふへほばびぶべぼぱ \
            ぴぷぺぽまみむめもやゆよらりるれろわゐゑをんっ"

KATAKANA = "ァアイウエオカキクケコサシスセソタチツテトナニヌネノハヒフヘホ \
            マミムメモヤユヨラリルレロワンガギグゲゴザジズゼゾダヂヅデド \
            バビブベボパピプペポ"

SYMBOLS = "・"

list_kanji = []
list_kana = []

# Create a text file
f= open("core_12-15k.txt","w+")

# Database config
FrequencyDBSubfolderName = "Bulk_Generate_Vocab_Frequency\\freq.sqlite"
FrequencyDBLocation = ""+os.path.dirname(__file__) + FrequencyDBSubfolderName
FQdb = sqlite3.connect(FrequencyDBLocation)

for frequency in range(30000,30100):
    mQuery = "select expression from Dict where freq =\"" + str(frequency) + "\";"
    cursor = FQdb.cursor()
    cursor.execute(mQuery)
    result = cursor.fetchone() #retrieve the first row
    print(str(result) + "," + str(frequency))
    '''if result:
        print(result[0])
        f.write(result[0] + "," + str(frequency) + '\n')
        if result[0][0] not in (HIRAGANA + KATAKANA + SYMBOLS):
            list_kanji.append(result[0][0])
            print(result[0])
            f.write(result[0] + '\n')
        else:
            list_kana.append(result[0][0])'''


FQdb.close()
f.close

    

