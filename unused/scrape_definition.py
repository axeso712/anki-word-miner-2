﻿#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
    Sanseido Definitions plugin for Anki
    pulls definitions from sanseido.net's デイリーコンサイス国語辞典

    Definition fetching adapted from rikaichan.js
    Field updating modified from Sentence_Gloss.py

    @author     = kqueryful
    @date       = 1/18/2015
    @version    = 1.0
"""
from bs4 import BeautifulSoup
from urllib.parse import quote
import re
from urllib.request import urlopen, Request
import random

#from nhk_pronunciation import multi_lookup
#from japanese.reading import MecabController

# Edit these field names if necessary ==========================================
expressionField = 'Vocabulary-Kanji'
definitionField = 'Sanseido'
productionField = 'ProductionDef'
# ==============================================================================

#mecab = MecabController()

# Fetch definition from Sanseido ===============================================
def download_definition(term):
    searched = re.search(r'^[^\[]+',term)
    if searched:
        term = searched.group(0)
    defText = ""
    pageUrl = "http://www.sanseido.biz/User/Dic/Index.aspx?TWords=" + quote(term.encode('utf-8')) + "&st=0&DailyJJ=checkbox"
    response = urlopen(pageUrl)
    soup = BeautifulSoup(response, features='lxml')
    NetDicBody = soup.find('div', class_ = "NetDicBody")
    if NetDicBody != None:
        defFinished = False

        for line in NetDicBody.children:
            if line.name == "b":
                if len(line) != 1:
                    for child in line.children:
                        if child.name == "span":
                            defFinished = True
            if defFinished:
                break

            if line.string != None and line.string != u"\n":
                defText += line.string

    defText = re.sub(r"［(?P<no>[２-９]+)］", r"<br/><br/>［\1］", defText)
    if defText:
        defText = u"　・<b>" + term + "</b>: " + defText

    return re.sub(r"（(?P<num>[２-９]+)）", r"<br/>（\1）", defText)
