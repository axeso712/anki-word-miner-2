import sys
import os

sys.path.append(os.getcwd() + "\\anki") # Terminal must run from folder with anki-foler
from anki.storage import Collection # If module not found error occurs pip install the module

PROFILE_HOME = "C:/Users/Axel/AppData/Roaming/Anki2/User 1"
CPATH = os.path.join(PROFILE_HOME, "collection.anki2")
DECK_NAME = "2. 語彙" 

col = Collection(CPATH, log=True)
num_edited = 0

for cid in col.findNotes("deck:" + "\'" + DECK_NAME + "\'"):
    db_note = col.getNote(cid) # Get note from deck
    if 'font' in db_note.fields[0] and 'font' not in db_note.fields[1]: # Check the image field if empty
        print(f"Old 0: {db_note.fields[0]}") 
        print(f"Old 1: {db_note.fields[1]}") 

        plain = db_note.fields[1]
        accent = db_note.fields[0]

        db_note.fields[0] = plain
        db_note.fields[1] = accent

        print(f"New 0: {db_note.fields[0]}") 
        print(f"New 1: {db_note.fields[1]}") 

        num_edited = num_edited + 1
        db_note.flush()
    col.save()

print("num_edited: " + str(num_edited))


