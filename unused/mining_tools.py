import sys 
import re 
import os
import regex as re
import requests
import MeCab
from urllib.parse import quote
from bs4 import BeautifulSoup


def tech_vocab_scraper():
  file1 = open('tech_vocab.txt', 'r')
  lines = file1.readlines()
  word_list = []
  for li in lines:
    en_part_1 = re.search(r'\w(.*?)\s[(\s)(\p{Hiragana}\p{Katakana}\p{Han}(\uFF5F-\uFF9F))]', li)
    jp_part_1 = re.search(r"[\p{Hiragana}\p{Katakana}\p{Han}(\uFF5F-\uFF9F)](.*?)(\s\s)", li)
    en_part_2 = re.search(r'(\s\s)[a-z](.*?)(\s\s)', li)
    jp_part_2 = re.search(r"[\p{Hiragana}\p{Katakana}\p{Han}(\uFF5F-\uFF9F)\S]*$", li)
    if jp_part_2.group() == '': # Blank line
      continue
    elif not en_part_2: # Single pair of words
      word_list.append([en_part_1.group().strip(), jp_part_2.group().strip()])
    else: # Two pairs of words
      word_list.append([en_part_1.group().strip(), jp_part_1.group().strip()])
      word_list.append([en_part_2.group().strip(), jp_part_2.group().strip()])
  file1.close()
  return word_list


def word_freq_scraper():
  word_list = []
  with open('words_short.html','r') as html_file:
    soup = BeautifulSoup(html_file, 'html.parser')
  match = soup.find_all('li')
  for item in match:
    for bold in item.b:
        word_list.append(bold)
  return __remove_garbage(word_list)

def n0_list_parser(file_name):
  file1 = open(file_name, 'r')
  lines = file1.readlines()
  lines_2 = [x.replace('\n', '') for x in lines]
  #print(lines_2)
  return lines_2


def n1_list_scraper():
  word_list = []
  with open('JLPT_N1.html', 'r') as html_file:
    print('opening file')
    soup = BeautifulSoup(html_file, 'html.parser')
    rows = soup.find_all('tr')
    for row in rows:
      #print(f"\nThe row: {row}")
      word = []
      for cell in row.find_all('a'):
        #print(f"\nThe cell: {cell.text}")
        word.append(cell.text)
      word_list.append(word)
    return word_list


def wiki_scraper(word):
  text_main = ''
  text_headers = ''
  url = f"https://ja.wikipedia.org/wiki/{quote(word)}"
  response = requests.get(url)
  if response.status_code == 200:
    soup = BeautifulSoup(response.content, 'html.parser')
  else:
    print("Could not get url")
    return ''

  soup1 = soup.find('div', id="mw-content-text") # Main content

  for head in soup1.find_all('span', class_='mw-headline'):
    text_headers = text_headers + head.text

  for para in soup1.find_all('p'):
    text = para.text
    text = re.sub(r"\[\d+\]","",text) # Strips out [x]-footnotes 
    text_main = text_main + text
  return text_headers + text_main


def mecab_parser(data):
  """
  Global: (String) --> List of strings
  Uses Mecab to separate words and return list of Word
  """
  words = []

  parse = MeCab.Tagger().parse(data)
  lines = parse.split('\n')
  items = (re.split('[\t,]', line) for line in lines)

  for item in items:
      if item[0] != 'EOS' and item[0] != '':
          words.append(item[0])
  return __remove_garbage(words)


def __remove_values_from_list(the_list, val):
   """
   Internal: (List) --> List
   """
   return [value for value in the_list if value != val]


def __remove_symbols(word_list):
  """
  Global: (List) --> List
  Removes words with letters in kanji field
  """
  for word in word_list:
    index = word_list.index(word)
    for char in word:  
        if char in "ABCDEFGHIJKLMNOPQRSTUVWXYZ \
                    abcdefghijklmnopqrstuvwxyz \
                    1234567890^{[]}。（）、「」一\
                    /%.\"β<>・×=;‐ασ)-​#:":
            word_list[index] = ''
  return __remove_values_from_list(word_list, '')

def __remove_single_kana(word_list):
  """
  Global: (List) --> List
  """ 
  for word in word_list:
    if word in "あいうえお \
                かきくけこ \
                がぎぐげご \
                さしすせそ \
                ざじずぜぞ \
                たちつてと \
                だぢづでど \
                なにぬねの \
                はひふへほ \
                びぶべぼぱ \
                ぴぷぺぽま \
                みむめもや \
                ゆよらりる \
                れろわゐゑ \
                をん一二三 \
                四五六七八 \
                九十 \
                " and len(word) == 1:
        word_list[word_list.index(word)] = ''
  return __remove_values_from_list(word_list, '')

def __remove_dupes(list_in):
    s = set(list_in)
    return list(s)

def __remove_garbage(word_list):
    word_list = __remove_single_kana(word_list)
    word_list = __remove_symbols(word_list)
    word_list = __remove_dupes(word_list)
    return word_list

def main():
  # Test n0
  #n0_list_parser('core_12-15k.txt')

  # Test word freq scraper
  """words = word_freq_scraper('words_short.html') 
  print(word_freq_scraper('words_short.html'))
  words = __remove_symbols(words)
  words = __remove_single_kana(words)
  words = __remove_symbols(words)
  print(words)"""

  # Test wiki scraper
  """data = wiki_scraper('利子')
  mecab_list = mecab_parser(data)
  for x in mecab_list: print(x)"""
 
  # Test jlpt scraper
  '''print(os.getcwd())
  print(os.path.dirname(os.path.abspath(__file__)))
  words = n1_list_scraper(10,40)
  for word in words:
    print(word)
  #print(words)'''

if __name__== "__main__" :
    main()