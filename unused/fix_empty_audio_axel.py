import sys
import os
from download_audio_all import audioDownload

sys.path.append(os.getcwd() + "\\anki")
from anki.storage import Collection

PROFILE_HOME = "C:/Users/Axel/AppData/Roaming/Anki2/User 1" #TODO find way to locate profile automatically
CPATH = os.path.join(PROFILE_HOME, "collection.anki2")
DECK_NAME = "deck:'2. 語彙::2. マイデッキ'" #TODO find way to ask user for deck

col = Collection(CPATH, log=True)

def main():
    for cid in col.findNotes(DECK_NAME):
        db_note = col.getNote(cid) # Get note from deck
        if len(db_note.fields) > 6:
            if db_note.fields[6] == '' and "no_sound" not in db_note.tags:
                (data, file_name) = audioDownload(db_note.fields[2], db_note.fields[0]) # audioDownload(kana, kanji)
                if data != None:
                    col.media.writeData(file_name, data)
                    db_note.fields[6] = u'[sound:{}]'.format(file_name)
                    print(f"New field: {db_note.fields[6]}")
                else:
                    print(f"Added no_sound tag to {db_note.fields[0]}\n")
                    db_note.tags.append("no_sound")
                db_note.flush()
            col.save()

def test_tags():
        for cid in col.findNotes(DECK_NAME):
            db_note = col.getNote(cid) # Get note from deck
            print(db_note.tags)
            print()
            #db_note.tags.append("test_tag")
            db_note.flush()
            col.save()

if __name__ == "__main__":
    main()

