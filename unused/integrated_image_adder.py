#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
    Description
"""
import urllib
from bs4.BeautifulSoup import BeautifulSoup
import os
import itertools
import json
import uuid

from PyQt4.QtCore import *
from PyQt4.QtGui import *
from anki.hooks import addHook
from anki.notes import Note
from aqt import mw # import the main window object (mw) from aqt

# Edit these field names if necessary ===============================
srcField = "Vocabulary-Kanji-Plain"
dstField = "Image"
# ===================================================================

REQUEST_HEADER = {
    'User-Agent': "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.134 Safari/537.36"}

# Fetch image ====================================================
def imageDownload(kanji, num_img):
    
    query = urllib.parse.quote(kanji)
    query = '+'.join(query.split())
    url = "https://www.google.co.in/search?q=%s&source=lnms&tbm=isch" % query

    response = urllib.urlopen(urllib.Request(url, headers=REQUEST_HEADER))
    soup = BeautifulSoup(response, 'html.parser')

    image_elements = soup.find_all("div", {"class": "rg_meta"})
    if not image_elements:
        return (None, "")
    metadata_dicts = (json.loads(e.text) for e in image_elements)
    link_type_records = ((d["ou"], d["ity"]) for d in metadata_dicts)
    link_type_records_slice = itertools.islice(link_type_records, num_img)

    for (url, image_type) in enumerate(link_type_records_slice):
        try:
            req = urllib.Request(url, headers=REQUEST_HEADER)
            resp = urllib.urlopen(req, timeout=10.0)
            raw_image = resp.read()
            extension = image_type if image_type in 'jpg png gif jpeg' else 'jpg'
            file_name = uuid.uuid4().hex + "." + extension
            return (raw_image, file_name)
        except urllib.error.URLError as e:
            raise e
        except Exception as e:
            raise e

# Update note =======================================================
def glossNote( f ):
    if f[ dstField ]: return
    (data, file_name) = imageDownload(f[ srcField ], 100)
    f[ dstField ] = '<img src="' + file_name + '" />'
    mw.col.media.writeData(file_name, data)

def setupMenu( ed ):
    a = QAction( 'Regenerate Images', ed ) # create a new menu item, "Regenerate Images"
    ed.connect( a, SIGNAL('triggered()'), lambda e=ed: onRegenGlosses( e ) )
    ed.form.menuEdit.addAction( a )

def onRegenGlosses( ed ):
    n = "Regenerate Images"
    ed.editor.saveNow()
    regenGlosses(ed, ed.selectedNotes() )
    mw.requireReset()

def regenGlosses( ed, fids ):
    mw.progress.start( max=len( fids ) , immediate=True)
    for (i,fid) in enumerate( fids ):
        mw.progress.update( label='Generating Images...', value=i )
        f = mw.col.getNote(id=fid)
        try: glossNote( f )
        except:
            pass
            """import traceback
            print 'image generation failed:'
            traceback.print_exc()"""
        try: f.flush()
        except:
            raise Exception()
            #sleep(10*random.random())
        ed.onRowChanged(f,f)
    mw.progress.finish()

addHook( 'browser.setupMenus', setupMenu )
