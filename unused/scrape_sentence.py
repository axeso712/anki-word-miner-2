from bs4 import BeautifulSoup
from urllib.request import urlopen, Request, urlretrieve
from urllib.parse import quote
from selenium import webdriver
import re

DIR2 = 'C:/tmp/'
REQUEST_HEADER = {
    'User-Agent': "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.134 Safari/537.36"}

def get_sentence(word):

    url = f"https://tatoeba.org/eng/sentences/search?query={quote(word)}&from=jpn&to=eng"
    
    # Getting response and soup (trad way)
    response = urlopen(Request(url, headers=REQUEST_HEADER))
    soup = BeautifulSoup(response, 'html.parser')

    # Selenium code (cool way)
    driver = webdriver.Chrome(executable_path='C:/chrome_driver/chromedriver.exe')
    driver.get(url)
    html = driver.page_source

    # Writing page source to text file
    text_file = open(DIR2 + "soup.txt", "w", encoding="utf-8")
    n = text_file.write(soup.text)
    text_file.close()

    text_file = open(DIR2 + "selenium.txt", "w", encoding="utf-8")
    n = text_file.write(html)
    text_file.close()
    
    # Using regex to find sentence
    sentence_matches = re.findall(r'(?:text)(.*)(?:jpn)', html)
    print(sentence_matches)
    
get_sentence('バラ')