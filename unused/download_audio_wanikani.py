from urllib.request import urlopen, Request, URLError, urlretrieve, HTTPError
from urllib.parse import quote
from bs4 import BeautifulSoup
import logging
import hashlib
import re

DIR2 = 'C:/tmp/'
BASE_URL = 'https://www.wanikani.com/vocabulary/'

def configure_logging():
    logger = logging.getLogger()
    logger.setLevel(logging.INFO)
    handler = logging.StreamHandler()
    handler.setFormatter(
        logging.Formatter('[%(asctime)s %(levelname)s %(module)s]: %(message)s'))
    logger.addHandler(handler)
    Filehandler = logging.FileHandler("C:/users/public/image_downloader_log.txt") #Path to your LOG FILE.
    Filehandler.setFormatter(
        logging.Formatter('[%(asctime)s %(levelname)s %(module)s]: %(message)s'))
    logger.addHandler(Filehandler)
    return logger

logger = configure_logging()

def audioDownloadWani(kanji):
    kanji = re.sub('<[^<]+?>', '', kanji) # Remove trailing html

    url = BASE_URL + quote(kanji)

    # Souping
    response = urlopen(Request(url))
    soup = BeautifulSoup(response, 'html.parser')
    logger.info('Getting sound elements...')
    mp3_url_matches = re.findall(r'(?:http(?:s?):)(?:[/|.|\w|\s|-])*\.(?:mp3)',str(soup))
    #print(f"Url matches: {mp3_url_matches}")
    if mp3_url_matches:
        url = mp3_url_matches[0] # We go with the first mp3 (lady voice)
    else:
        logger.info(f'No audio found for {kanji}...')
        return (None,None)

    # Filename
    filename = u'wanikani_{}'.format(kanji)
    filename += u'.mp3'
    logger.info(f"Filename: {filename}")

    # Get file from url
    try:
        logger.info(f'Opening url for {kanji}...')
        resp = urlopen(url)
        urlretrieve(url, DIR2 + filename) # Save file locally to DIR2
        raw_sound = resp.read()
        return (raw_sound, filename)
    except URLError:
        logger.info(f'Exception occured returning (None,None)...')
        return (None,None)

if __name__ == "__main__":
    print("testing...")
    audioDownloadWani('男')
    audioDownloadWani('ここどここ')
