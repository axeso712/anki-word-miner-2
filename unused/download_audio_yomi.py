from urllib.request import urlopen, Request, URLError, urlretrieve
from urllib.parse import quote
import logging
import hashlib
import re

DIR2 = 'C:/tmp/'
BASE_URL = 'http://assets.languagepod101.com/dictionary/japanese/audiomp3.php?kanji='

def configure_logging():
    logger = logging.getLogger()
    logger.setLevel(logging.INFO)
    handler = logging.StreamHandler()
    handler.setFormatter(
        logging.Formatter('[%(asctime)s %(levelname)s %(module)s]: %(message)s'))
    logger.addHandler(handler)
    Filehandler = logging.FileHandler("C:/users/public/image_downloader_log.txt") #Path to your LOG FILE.
    Filehandler.setFormatter(
        logging.Formatter('[%(asctime)s %(levelname)s %(module)s]: %(message)s'))
    logger.addHandler(Filehandler)
    return logger

logger = configure_logging()

def audioDownload(kana, kanji):
    kana = re.sub('<[^<]+?>', '', kana) # Remove trailing html
    kanji = re.sub('<[^<]+?>', '', kanji) # Remove trailing html

    url = BASE_URL + quote(kanji)

    # Filename
    filename = u'yomichan_{}'.format(kana)
    if kanji:
        filename += u'_{}'.format(kanji)
    filename += u'.mp3'
    logger.info(f"Filename: {filename}")

    # Get file from url
    if kana:
        url += u'&kana={}'.format(quote(kana))
    try:
        logger.info(f'Opening url for {kana}...')
        resp = urlopen(url)
        #urlretrieve(url, DIR2 + filename) # Save file locally to DIR2
        raw_sound = resp.read()
        return (raw_sound, filename)
    except URLError:
        logger.info(f'Exception occured returning (None,None)...')
        return (None,None)

def audioIsPlaceholder(data):
    m = hashlib.md5()
    m.update(data)
    return m.hexdigest() == '7e2c2f954ef6051373ba916f000168dc'

if __name__ == "__main__":
    print("testing...")
    audioDownload('およぐ','泳ぐ')
