import json

def getWordInfo(kanji_word):
    with open("userscripts/freq db/final_dict.json","r",encoding='utf-8') as dict_file:
        data = json.load(dict_file)
    word_info_tuple = (data["reading"].get(kanji_word), \
                        data["vocabulary position"].get(kanji_word), \
                        data["translation"].get(kanji_word), \
                        data["frequency"].get(kanji_word), \
                        data["definition"].get(kanji_word))
    return word_info_tuple

if __name__ == "__main__":
    # Testing:
    jap_word_tuple = getWordInfo('言葉') #Existing word, returns ('ことば', 'noun'. etc...)
    none_word_tuple = getWordInfo('hejsan') #Random word, returns (None, None, etc...)

    if jap_word_tuple:
        print(jap_word_tuple)

    if none_word_tuple:
        print(none_word_tuple)



