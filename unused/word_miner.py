import sys
import os
import codecs
import re
import json
from urllib.parse import quote
from dictionary import query_jisho, render_word, render_reading, render_parts_of_speech, render_definition
from image_downloader import imageDownload
from mining_tools import word_freq_scraper, tatoeba_sentence, wiki_scraper 
from mining_tools import mecab_parser, tech_vocab_scraper, n1_list_scraper,n0_list_parser
from audio_helpers import audioBuildFilename, audioDownload, audioIsPlaceholder
from sanseido_definition import download_definition

sys.path.append("../anki") 
from anki.storage import Collection

PROFIL = 'Axel'
#PROFIL = 'Philip'

if PROFIL == 'Axel':
  PROFILE_HOME = "C:/Users/axeso/AppData/Roaming/Anki2/User 1"
  CPATH = os.path.join(PROFILE_HOME, "collection.anki2")
  MEDIA = 'C:/Users/axeso/AppData/Roaming/Anki2/User 1/collection.media'
  DUPE_QUERY = "deck:'2. 語彙' "

if PROFIL == 'Philip':
  PROFILE_HOME = "C:/Users/Philip Welin-Berger/AppData/Roaming/Anki2/User 1"
  CPATH = os.path.join(PROFILE_HOME, "collection.anki2")
  MEDIA = 'C:/Users/Philip Welin-Berger/AppData/Roaming/Anki2/User 1/collection.media'
  DUPE_QUERY = "deck:3言 "


class Word:

    def __init__(self, kanji, reading, english, word_pos, image_file, example, audio_file, definition):
      self.kanji = kanji
      self.reading = reading
      self.english = english
      self.word_pos = word_pos
      self.image_file = image_file
      self.example = example
      self.audio_file = audio_file
      self.definition = definition

    def add_jisho(self):
      response = query_jisho(self.kanji)
      if self.reading == '':
        self.reading = render_reading(response[1])
      if self.word_pos == '':
        self.word_pos = render_parts_of_speech(response[1])
      if (self.english == ''):
        self.english = render_definition(response[1])

    def add_audio(self, col):
      if self.audio_file == '':
        data = audioDownload(self.reading, self.kanji)
        if data is not None and not audioIsPlaceholder(data):
          filename = audioBuildFilename(self.reading, self.kanji)
          self.audio_file = u'[sound:{}]'.format(filename)
          col.media.writeData(filename, data)
  
    def add_image(self, col, num_img):
      if self.image_file == '':
        (data, file_name) = imageDownload(self.kanji, num_img)
        if data == None:
          print(f"\nNo data returned for {self.kanji}")
        else:
          self.image_file = '<img src="' + file_name + '" />'
          col.media.writeData(file_name, data)
    
    def add_definition(self):
      if self.definition == '':
        self.definition = download_definition(self.kanji)

    def add_example(self):
      if self.example == '':
        self.example = tatoeba_sentence(self.kanji).strip()


def addNote(deckName, modelName, word, col):
  if col is None:
    return

  deck = col.decks.byName(deckName) 
  modelBasic = col.models.byName(modelName)
  col.decks.current()['mid'] = modelBasic['id']
  note = col.newNote()
  note.model()['did'] = deck['id']

  tags = "auto-generated"
  note.tags = col.tags.canonify(col.tags.split(tags))
  m = note.model()
  m['tags'] = note.tags
  col.models.save(m)

  is_dupe = False

  if PROFIL == 'Axel':
    note.fields[0] = word.kanji #Vocabulary-Kanji-Plain
    note.fields[1] = word.kanji #Vocabulary-Kanji
    note.fields[2] = word.reading #Vocabulary-Kana
    note.fields[3] = word.english #Vocabulary-English
    note.fields[4] = word.word_pos #Vocabulary-Pos
    note.fields[6] = word.audio_file #Vocabulary-Audio
    note.fields[7] = word.image_file #Image
    note.fields[8] = word.definition #Sanseido
    note.fields[11] = word.example #Expression

  if PROFIL == 'Philip':
      note.fields[0] = word.kanji
      note.fields[3] = word.reading
      note.fields[4] = word.english
      note.fields[5] = word.image_file
      note.fields[6] = word.audio_file
      note.fields[7] = word.word_pos
      note.fields[9] = word.example
      note.fields[20]= word.definition
  
  for cid in col.findNotes(DUPE_QUERY + word.kanji):
      db_note = col.getNote(cid)
      if db_note.fields[0] == word.kanji:
          is_dupe = True
          print(f"\nDupe found for: {word.kanji}")
          break
  if not is_dupe:
    __print_word(word)
    col.addNote(note)
  col.save()


def __add_fields(word, col):
  word.add_jisho()
  #word.add_image(col, 100)
  word.add_audio(col)
  #word.add_example() # <--fixa denna TODO
  word.add_definition()


def __print_word_list(word_list):
  for word in word_list:
    __print_word(word)


def __print_word(word):
  print(f"\n[Kanji: {word.kanji},\nKana: {word.reading},")
  print(f"English: {word.english},\nWord_pos: {word.word_pos},")
  print(f"Image source: {word.image_file},\nSentence: {word.example},")
  print(f"Definition: {word.definition},\nImage source: {word.image_file}]")


def __add_one_word(deck, model, kanji_string):
  w = Word(kanji_string, '', '', '','', '', '', '')
  col = Collection(CPATH, log=True)
  __add_fields(w, col)
  addNote(deck, model, w, col)


def __add_one_sentence(deck, model, sentence_string):
  sentence_list = mecab_parser(sentence_string)
  col = Collection(CPATH, log=True)
  for word in sentence_list:
    w = Word(word, '', '', '', '', '', '', '')
    __add_fields(w, col)
    addNote(deck, model, w, col)


def __add_list_of_words(name, deck, model, start_index, end_index, specifier):

  if name == "N0":
    word_list = n0_list_parser('core_10-15k.txt')
    col = Collection(CPATH, log=True)
    for i in range(len(word_list)):
      word = word_list[i]
      new_word = Word(word, '', '', '', '', '', '', '')
      __add_fields(new_word, col)
      #__print_word(new_word)
      addNote(deck, model, new_word, col)

  if name == 'N1':
    word_list = n1_list_scraper() 
    print(len(word_list)) 
    if end_index > len(word_list):
      end_index = len(word_list)
    col = Collection(CPATH, log=True)
    for i in range(start_index, end_index):
      word = word_list[i]
      if len(word) == 3:
        new_word = Word(word[0],word[1],word[2],'','','','','')
      else:
        new_word = Word(word[0],word[0],word[1],'','','','','')
      __add_fields(new_word, col)
      addNote(deck, model, new_word, col)

  if name == 'Tech':
    word_list = tech_vocab_scraper()
    if end_index > len(word_list):
      end_index = len(word_list) - 1
    col = Collection(CPATH, log=True)
    for i in range(start_index, end_index):
      word = word_list[i]
      new_word = Word(word[1], '', word[0], '', '', '','','')
      __add_fields(new_word, col)
      addNote(deck, model, new_word, col)

  if name == 'Wiki':
    word_list = mecab_parser(wiki_scraper(specifier))
    if end_index > len(word_list):
      end_index = len(word_list) - 1
    col = Collection(CPATH, log=True)
    for i in range(start_index, end_index):
      word = word_list[i]
      new_word = Word(word[0], '', '', '', '', '', '', '')
      __add_fields(new_word, col)
      addNote(deck, model, new_word, col)


def main():
  #__add_one_word('Test', 'Japanese Vocab', '恋人')
  #__add_one_sentence('Test', 'Japanese Vocab', 'これはペンです。そして豚の様子')
  #__add_list_of_words('Wiki', 'Test', 'Japanese Vocab', 0, 10,'軍隊')
  __add_list_of_words('N0', '2. 語彙::3. N0', 'Japanese Vocab', 0, 10,'')
  #__add_list_of_words('Tech', '2. 語彙::基礎工業英単語', 'Japanese Vocab', 0, 10,'')
  #__add_list_of_words('Wiki', '3言:: 自分::ASK1 Words ', 'Japanese Core 2000', 0, 10,'軍隊')
  __add_list_of_words('N1', '3言:: 自分::ASK1 Words ', 'Japanese Core 2000', 3200,9999,'')
  #__add_list_of_words('Tech', '3言:: 自分::ASK1 Words ', 'Japanese Core 2000', 0, 10,'')


if __name__== "__main__" :
    main()

    