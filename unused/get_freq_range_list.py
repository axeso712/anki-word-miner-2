import json

def getFreqList(freq_range):
    freq_list = []
    with open("userscripts/freq db/final_dict.json","r",encoding='utf-8') as dict_file:
        data = json.load(dict_file)
    for i in freq_range:
        freq_list.append(data['reverse frequency'].get(str(i)))
    return freq_list

if __name__ == "__main__":
    # Testing:
    test_list = getFreqList(range(300,350))
    print(test_list)



