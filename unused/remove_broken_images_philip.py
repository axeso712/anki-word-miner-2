import sys
import os
from download_image import imageDownload

sys.path.append(os.getcwd() + "\\anki") # Terminal must run from folder with anki-foler
from anki.storage import Collection # If module not found error occurs pip install the module

PROFILE_HOME = "C:/Users/Philip Welin-Berger/AppData/Roaming/Anki2/User 1" #TODO find way to locate profile automatically
CPATH = os.path.join(PROFILE_HOME, "collection.anki2")
DECK_NAME = "3言:: 自分::ASK1 Words" #TODO find way to ask user for deck

col = Collection(CPATH, log=True)
num_broken_images = 0

for cid in col.findNotes(DECK_NAME):
    db_note = col.getNote(cid) # Get note from deck
    if db_note.fields[4] != '':
        if db_note.fields[4][-5] == '.': # Check the image field if empty
            #print(db_note.fields[4])
            db_note.fields[4] = ''
            num_broken_images += 1
            db_note.flush()
    col.save()

print("Broken images: " + str(num_broken_images))


