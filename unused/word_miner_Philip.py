import sys, os, codecs, re, json
from urllib.parse import quote
from dictionary import query_jisho, render_word, render_reading, render_parts_of_speech, render_definition
from image_downloader import imageDownload
from mining_tools import word_freq_scraper, tatoeba_sentence, wiki_scraper, mecab_parser, tech_vocab_scraper, n1_list_scraper
from audio_helpers import audioBuildFilename, audioDownload, audioIsPlaceholder
from sanseido_definition import fetchDef

# Get anki path and import
sys.path.append("../anki") 
from anki.storage import Collection

##############
## Constants #
##############

#PROFIL = 'Axel'
PROFIL = 'Philip'

# Axel
if PROFIL == 'Axel':
  PROFILE_HOME = "C:/Users/axeso/AppData/Roaming/Anki2/User 1"
  CPATH = os.path.join(PROFILE_HOME, "collection.anki2")
  MEDIA = 'C:/Users/axeso/AppData/Roaming/Anki2/User 1/collection.media'
  DUPE_QUERY = "deck:'2. 語彙' "
  pass
print(os.getcwd())
print(os.path.dirname(os.path.abspath(__file__)))


# Philip
if PROFIL == 'Philip':
  PROFILE_HOME = "C:/Users/Philip Welin-Berger/AppData/Roaming/Anki2/User 1"
  CPATH = os.path.join(PROFILE_HOME, "collection.anki2")
  MEDIA = 'C:/Users/Philip Welin-Berger/AppData/Roaming/Anki2/User 1/collection.media'
  DUPE_QUERY = "deck:3言 "


################
## Word class ##
################

class Word:

    def __init__(self, kanji, reading, english, word_pos, image_file, example, audio_file, definition):
      self.kanji = kanji
      self.reading = reading
      self.english = english
      self.word_pos = word_pos
      self.image_file = image_file
      self.example = example
      self.audio_file = audio_file
      self.definition = definition

    def get_jisho(self):
      response = query_jisho(self.kanji) #response = (message_back, definition)
      if self.reading == '':
        self.reading = render_reading(response[1])
      if self.word_pos == '':
        self.word_pos = render_parts_of_speech(response[1])
      if (self.english == ''):
        self.english = render_definition(response[1])

    def get_audio(self, col):
      data = audioDownload(self.reading, self.kanji)
      if data is not None and not audioIsPlaceholder(data):
        filename = audioBuildFilename(self.reading, self.kanji)
        self.audio_file = u'[sound:{}]'.format(filename)
        col.media.writeData(filename, data)
  
    def get_image(self, col, num_img):
      #self.image_file = run_image_downloader(quote(self.kanji),MEDIA_PATH, 1)
      (data, file_name) = imageDownload(self.kanji, num_img)
      if data == None:
        print(f"\nNo data returned for {self.kanji}")
      else:
        self.image_file = '<img src="' + file_name + '" />'
        col.media.writeData(file_name, data)
    
    def get_example(self):
      self.example = tatoeba_sentence(self.kanji)

    def get_definition(self):
      self.definition = fetchDef(self.kanji)

#
# Anki
#

class Anki:

  def addNote(self, deckName, modelName, word, col):
    collection = col
    if collection is None:
      return
    
    deck = col.decks.byName(deckName) 
    modelBasic = col.models.byName(modelName)
    col.decks.current()['mid'] = modelBasic['id']
    note = col.newNote()
    note.model()['did'] = deck['id']

    if PROFIL == 'Axel':
      note.fields[0] = word.kanji #Vocabulary-Kanji-Plain
      note.fields[1] = word.kanji #Vocabulary-Kanji
      note.fields[2] = word.reading #Vocabulary-Kana
      note.fields[3] = word.english #Vocabulary-English
      note.fields[4] = word.word_pos #Vocabulary-Pos
      note.fields[6] = word.audio_file #Vocabulary-Audio
      note.fields[7] = word.image_file #Image
      note.fields[8] = word.definition #Sanseido
      note.fields[11] = word.example #Expression

    if PROFIL == 'Philip':
      note.fields[0] = word.kanji
      note.fields[3] = word.reading
      note.fields[4] = word.english
      note.fields[5] = word.image_file
      note.fields[6] = word.audio_file
      note.fields[7] = word.word_pos
      note.fields[9] = word.example
      note.fields[20]= word.definition
    
    tags = "Tech auto-generated"
    note.tags = col.tags.canonify(col.tags.split(tags))
    m = note.model()
    m['tags'] = note.tags
    col.models.save(m)

    dupe = False

    for cid in col.findNotes(DUPE_QUERY + word.kanji):
        db_note = col.getNote(cid)
        #print(f"\nComparing {word.kanji} and {db_note.fields[0]}")
        if db_note.fields[0] == word.kanji:
            dupe = True
            print(f"\nDupe found for: {word.kanji}, not adding...")
            break
    if not dupe:
      print(f"\nNo dupes found, adding: {word.kanji}")
      print(f"[\nKanji: {word.kanji},\nKana: {word.reading},\nEnglish: {word.english},\nWord_pos: {word.word_pos},\nImage source: {word.image_file},\nSentence: {word.example},\nDefinition: {word.definition}]")
      col.addNote(note)
    
    col.save()

def __add_fields(word, col):
  word.get_jisho()
  word.get_image(col, 100)
  word.get_audio(col)
  word.get_example()
  word.get_definition()

def __print_word_list(word_list):
  for word in word_list:
    __print_word(word)
    
def __print_word(word):
  print(f"[\nKanji: {word.kanji},\nKana: {word.reading},\nEnglish: {word.english},\nWord_pos: {word.word_pos},\nImage source: {word.image_file},\nSentence: {word.example},\nDefinition: {word.definition}]")

def main():
  anki = Anki()

  #Test 1.0: one word
  '''w = Word('蛋白質', '', '', '','', '','','')
  __add_fields(w, col)
  anki.addNote('Test', 'Japanese Vocab', w, col)'''

  # Test 1.1: two words
  """col = Collection(CPATH, log=True) 
  w = Word('恋人', '', '', '','', '')
  add_fields(w)
  addNotes(w, col)
  w2 = Word('人', '', '', '','', '')
  add_fields(w2)
  addNotes(w2, col)"""

  #Test 1.5: one sentence
  """sentence = "僕は代数学を教えてもらいたい。鶴が好きだ"
  list_ = mecab_parser(sentence)
  print(list_)
  for word in list_:
    w = Word(word, '', '', '','', '','','')
    __add_fields(w, col)
    anki.addNote('Auto', 'Japanese Vocab', w, col)"""

  # Test 2: A collection of words
  '''word_list = word_freq_scraper()
  #here = os.path.dirname(os.path.abspath(__file__))
  print(f"The word list: {word_list}")
  for kanji_string in word_list:
    new_word = Word(kanji_string,'','','','','')
    __add_fields(new_word)
    print_word(new_word)
    #addNote(new_word, col)'''

  # Tests 2.5: N1 collection
  word_list = n1_list_scraper(0,1000)
  col = Collection(CPATH, log=True)
  #print(f"The word list: {word_list}")
  for set_ in word_list:
    if len(set_) == 3:
      new_word = Word(set_[0],set_[1],set_[2],'','','','','')
    else:
      new_word = Word(set_[0],set_[0],set_[1],'','','','','')
    __add_fields(new_word, col)
    __print_word(new_word)
    anki.addNote('3言:: 自分::ASK1 Words ', 'Japanese Core 2000', new_word, col)

  # Test 3: Add tech vocab
  #word_list = tech_vocab_scraper(20)
  """word_list = tech_vocab_scraper(1490,9999)
  print(f"The word list: {word_list}")
  for word in word_list:
    new_word = Word(word[1], '', word[0], '', '', '','','')
    __add_fields(new_word, col)
    anki.addNote('2. 語彙::基礎工業英単語', 'Japanese Vocab', new_word, col)"""
    

  # Test 4:
  """col = Collection(CPATH, log=True)
  word_list = mecab_parser(wiki_scraper('利子'))
  print(f"The word list: {word_list}")
  for word in word_list:
    new_word = Word(word[0], '', '', '', '', '')
    __add_fields(new_word)
    print_word(new_word)
    #addNote(new_word, col)"""

if __name__== "__main__" :
    main()

    