import sys, os
from get_word_info import getWordInfo
from get_sentence import getWordSentence
from download_audio_yomi import audioDownload, audioIsPlaceholder
from download_image import imageDownload
from get_freq_range_list import getFreqList

sys.path.append(os.getcwd() + "\\anki")
from anki.storage import Collection

CWD = os.getcwd() # Get our working directory

# Philip constants
PROFILE_HOME = "C:/Users/Philip Welin-Berger/AppData/Roaming/Anki2/User 1"
CPATH = os.path.join(PROFILE_HOME, "collection.anki2")
MAIN_DECK = "3言"
DECK = '3言:: 自分::ASK1 Words'
MODEL = 'Japanese Core 2000'

# Get the collection
col = Collection(CPATH, log=True)
os.chdir(CWD) # Change back to current working directory

def addNote(deckName, modelName, entry):
    '''Adds entry as a note in anki plus image from google and audio from japanesepos101'''

    # Get deck and model
    deck = col.decks.byName(deckName)
    modelBasic = col.models.byName(modelName)
    col.decks.current()['mid'] = modelBasic['id']

    # Make new note
    note = col.newNote()
    note.model()['did'] = deck['id']

    # Set tag to auto-generated
    tags = "auto-generated core10k"
    note.tags = col.tags.canonify(col.tags.split(tags))
    m = note.model()
    m['tags'] = note.tags
    col.models.save(m)

    '''# Fields
    note.fields[0] = entry['kanji'] # Vocabulary-Kanji-Plain
    note.fields[1] = entry['kanji'] # Vocabulary-Kanji
    note.fields[2] = entry['kana'] # Vocabulary-Kana
    note.fields[3] = entry['en'] # Vocabulary-English
    note.fields[4] = entry['pos'] # Vocabulary-Pos'''
   
    # Phields
    note.fields[0] = entry['kanji']
    note.fields[3] = entry['kana']
    note.fields[2] = entry['en']
    note.fields[5] = entry['pos']
    if entry['def']:
        note.fields[9] = entry['def'] # Sanseido
    if entry['sentence']:
        note.fields[6] = entry['sentence'] # Expression

    # Dupe checking for field 0 against 'kanji'
    isDupe = False
    for cid in col.findNotes("deck:" + "\'" + MAIN_DECK + "\'"):
        if col.getNote(cid).fields[0] == entry['kanji']:
            isDupe = True
            print(f"Dupe found @ {entry['kanji']}")
            break

    # Add note to collection
    if not isDupe:
        print(f"No dupe found @ {entry['kanji']}")
        addAudio(note)
        addImage(note)
        col.addNote(note)
        print(f"Added {note.fields[0]}!")

    # Save collection
    col.save()


def addAudio(db_note):
    '''Adds audio to field 6 from kanji field 0 and kana in field 2'''

    (data, file_name) = audioDownload(db_note.fields[3], db_note.fields[0])
    # If the audio is placeholder means it is not available
    if data is not None and not audioIsPlaceholder(data):
        col.media.writeData(file_name, data)
        db_note.fields[17] = u'[sound:{}]'.format(file_name)
        print(f"New field: {db_note.fields[17]}")


def addImage(db_note):
    '''Adds image to field 7 of db_note from the kanji in field 0'''

    (data, file_name) = imageDownload(db_note.fields[0]+'イラスト')
    if data:
        image_file = '<img src="' + file_name + '" />'
        col.media.writeData(file_name, data)
        db_note.fields[4] = image_file
        print(f"New field: {db_note.fields[4]}")


def createEntry(kanji_word):
    '''Returns a dictionary of the kanji word with kanji, kana, pos,
    english translation, frequency, definition and sentence'''

    info_tuple = getWordInfo(kanji_word)
    sentence = getWordSentence(kanji_word)
    if info_tuple[0]:
        return {'kanji': kanji_word, 'kana': info_tuple[0], 'pos': info_tuple[1], 'en': info_tuple[2], \
            'freq': info_tuple[3], 'def': info_tuple[4], 'sentence': sentence}
    else:
        return None


if __name__ == "__main__":
    print("Starting...")

    # For testing purposes
    # test_list = ['１つ', '空手', '言葉', 'hejsan']
    test_list_2 = getFreqList(range(6794,10000))

    # Testing adding of entries as notes in Anki
    for word in test_list_2:
        entry = createEntry(word)
        # Check if the word existed in dictionary (is not None)
        if entry:
            addNote(DECK, MODEL, entry)
