#!/usr/bin/env python
import sys, os, json, csv
from download_audio import audioDownload
from download_image import imageDownload

sys.path.append(os.getcwd() + "\\anki")
from anki.storage import Collection

CWD = os.getcwd() # Get our working directory

################
# Load Profile #
################
if len(sys.argv) > 1:
    PROFILE = sys.argv[1]
else:
    PROFILE = None

try:
    with open('userscripts/profile_' + PROFILE + '.json','r',encoding='utf-8') as profile_file:
        profile_data = json.load(profile_file)
except OSError as e:
    print(f'The profile for {PROFILE} was not found, is the name correct?')
    PROFILE = None
    profile_data = None
except TypeError as e:
    print(f"You have to input profile as an argument")
    PROFILE = None
    profile_data = None

#################
#  Profile data #
#################
if profile_data:
    PROFILE_HOME = profile_data['PROFILE_HOME']
    CPATH = os.path.join(PROFILE_HOME, "collection.anki2")
    MAIN_DECK = profile_data['MAIN_DECK']
    DECK = profile_data['DECK']
    MODEL = profile_data['MODEL']

    # Fields
    KANJI_1 = profile_data['kanji_field_1']
    KANJI_2 = profile_data['kanji_field_2']
    KANA = profile_data['kana_field']
    ENGLISH = profile_data['english_field']
    WORD_POS = profile_data['word_pos_field']
    AUDIO_FIELD = profile_data['audio']
    IMAGE_FIELD = profile_data['image']
    DEFINITION = profile_data['definition_field']
    EXAMPLE_SEN = profile_data['sentence_field']
    FREQ = profile_data['frequency']

    # Get the collection
    col = Collection(CPATH, log=True)
    os.chdir(CWD) # Change back to current working directory

##################
# Main functions #
##################

def addNote(deckName, modelName, entry):
    '''Adds entry as a note in anki 
    plus image from google and audio from 
    japanesepos101 or wanikani'''

    # Get deck and model
    deck = col.decks.byName(deckName)
    modelBasic = col.models.byName(modelName)
    col.decks.current()['mid'] = modelBasic['id']

    # Make new note
    note = col.newNote()
    note.model()['did'] = deck['id']

    # Find frequency range
    word_freq = int(entry['freq'])
    if word_freq <= 5000:
        freq_range = 'wordfreq1-5000'
    elif word_freq > 5000 and word_freq <= 10000:
        freq_range = 'wordfreq5001-10000'
    elif word_freq > 10000 and word_freq <= 15000:
        freq_range = 'wordfreq10001-15000'
    elif word_freq > 15000 and word_freq <= 20000:
        freq_range = 'wordfreq15001-20000'
    elif word_freq > 20000 and word_freq <= 25000:
        freq_range = 'wordfreq20001-25000'
    else:
        freq_range = 'wordfreq25000+'
    
    # Set tag to auto-generated and frequency range
    tags = "auto-generated" + " " + freq_range
    note.tags = col.tags.canonify(col.tags.split(tags))
    m = note.model()
    m['tags'] = note.tags
    col.models.save(m)

    # Fields
    note.fields[KANJI_1] = entry['kanji'] # Vocabulary-Kanji-Plain
    note.fields[KANJI_2] = entry['kanji'] # Vocabulary-Kanji
    note.fields[KANA] = entry['kana'] # Vocabulary-Kana
    note.fields[ENGLISH] = entry['en'] # Vocabulary-English
    note.fields[WORD_POS] = entry['pos'] # Vocabulary-Pos
    note.fields[DEFINITION] = entry['def'] # Sanseido
    note.fields[EXAMPLE_SEN] = entry['sentence'] # Expression
    note.fields[FREQ] = entry['freq'] # Frequency

    # Dupe checking
    isDupe = False
    for cid in col.findNotes("deck:" + "\'" + MAIN_DECK + "\'"):
        if col.getNote(cid).fields[KANJI_1] == entry['kanji']:
            isDupe = True
            print(f"Dupe found @ {entry['kanji']}")
            break

    # Add note to collection
    if not isDupe:
        print(f"No dupe found @ {entry['kanji']}")
        addAudio(note)
        addImage(note)
        col.addNote(note)
        print(f"Added {note.fields[KANJI_1]}!")

    # Save collection
    col.save()


def addAudio(db_note):
    (data, file_name) = audioDownload(db_note.fields[KANA], db_note.fields[KANJI_1])
    # If the audio is placeholder means it is not available
    if data != None:
        col.media.writeData(file_name, data)
        db_note.fields[AUDIO_FIELD] = u'[sound:{}]'.format(file_name)
        print(f"New field: {db_note.fields[AUDIO_FIELD]}")
    else:
        db_note.tags.append("no_sound")


def addImage(db_note):
    (data, file_name) = imageDownload(db_note.fields[KANJI_1]+'イラスト')
    if data:
        image_file = '<img src="' + file_name + '" />'
        col.media.writeData(file_name, data)
        db_note.fields[IMAGE_FIELD] = image_file
        print(f"New field: {db_note.fields[IMAGE_FIELD]}")


def getFreqList(freq_range):
    freq_list = []
    with open("userscripts/resources/final_dict.json","r",encoding='utf-8') as dict_file:
        data = json.load(dict_file)
    for i in freq_range:
        freq_list.append(data['reverse frequency'].get(str(i)))
    return freq_list


def getWordSentence(word):
    with open('userscripts/resources/jpn_sentences.tsv', encoding='utf-8') as tsvfile:
        reader = csv.reader(tsvfile, delimiter='\t')
        for row in reader:
            if word in row[2]:
                return row[2]
        return ""


def createEntry(kanji_word):
    # Open dictionary
    with open("userscripts/resources/final_dict.json","r",encoding='utf-8') as dict_file:
        data = json.load(dict_file) # Get data from json file
    sentence = getWordSentence(kanji_word)
    entry_dict = {  'kanji': kanji_word, \
                    'kana': data["reading"].get(kanji_word), \
                    'pos': data["vocabulary position"].get(kanji_word), \
                    'en': data["translation"].get(kanji_word), \
                    'freq': str(data["frequency"].get(kanji_word)), \
                    'def': data["definition"].get(kanji_word), \
                    'sentence': sentence}
    
    # Definition may be None if not found
    if entry_dict['def'] == None:
        entry_dict['def'] = ''

    # If no kana found the word is not in dictionary
    if entry_dict['kana'] == None:
        return None
    else:
        return entry_dict

if __name__ == "__main__" and PROFILE:
    print("Starting...")
    
    # For testing purposes
    #test_list = ['１つ','空手', 'hejsan']
    #test_list = getFreqList(range(9000,9002))

    # Prompting frequency
    start = int(input('Enter beginning of frequency range: '))
    end = int(input('Enter end of frequency range: '))
    if end > start and end != 0:
        word_list = getFreqList(range(start,end))

    # Iterate list and add notes
    for word in word_list:
        entry = createEntry(word)
        if entry:
            addNote(DECK, MODEL, entry)
