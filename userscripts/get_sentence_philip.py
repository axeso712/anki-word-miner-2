import csv

def getWordSentence(word):
    with open('jpn_sentences.tsv', encoding='utf-8') as tsvfile:
        reader = csv.reader(tsvfile, delimiter='\t')
        for row in reader:
            if word in row[2]:
                return row[2]
        return ""

if __name__ == "__main__":
    print(f"Testing word 空手: {getWordSentence('空手')}")
    print(f"Testing word hejsan: {getWordSentence('hejsan')}")
    print(f"Testing word 稲刈り: {getWordSentence('稲刈り')}")