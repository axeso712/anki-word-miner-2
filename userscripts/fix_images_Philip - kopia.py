import sys, os

sys.path.append("../anki") 
from anki.storage import Collection
from image_downloader import imageDownload

PROFILE_HOME = "C:/Users/Philip Welin-Berger/AppData/Roaming/Anki2/User 1"
CPATH = os.path.join(PROFILE_HOME, "collection.anki2")

col = Collection(CPATH, log=True)

num_empty = 0

for cid in col.findNotes("deck:'3言:: 自分::ASK1 Words '"):
    #print('scanning')
    db_note = col.getNote(cid)
    print(f'Field 0 is: {db_note.fields[0]}')
    #print(f'Field is: {db_note.fields[5]}')
    if len(db_note.fields) > 6:
        if '<img src="' in db_note.fields[7]:
            print(f'Is image: {db_note.fields[7]}')
            db_note.fields[5] = db_note.fields[7]
            if '<img src="' in db_note.fields[5]:
                db_note.fields[7] = ''
            print(f'Field 5 is: {db_note.fields[5]}')
            #db_note.fields[5] = db_note_fields[7]
            #print(f"New field: {db_note.fields[5]}")
            db_note.flush()
        else:
            print(f'Is not image: {db_note.fields[7]}')
    col.save()
print("Empty images: " + str(num_empty))

