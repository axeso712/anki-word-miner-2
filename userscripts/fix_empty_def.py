#!/usr/bin/env python
import sys, os, json
from get_word_info import getWordInfo

sys.path.append(os.getcwd() + "\\anki")
from anki.storage import Collection

CWD = os.getcwd() # Get our working directory

################
# Load Profile #
################
if len(sys.argv) > 1:
    PROFILE = sys.argv[1]
else:
    PROFILE = None

try:
    with open('userscripts/profile_' + PROFILE + '.json','r',encoding='utf-8') as profile_file:
        profile_data = json.load(profile_file)
except OSError as e:
    print(f'The profile for {PROFILE} was not found, is the name correct?')
    PROFILE = None
    profile_data = None
except TypeError as e:
    print(f"You have to input profile as an argument")
    PROFILE = None
    profile_data = None

#################
#  Profile data #
#################
if profile_data:
    PROFILE_HOME = profile_data['PROFILE_HOME']
    CPATH = os.path.join(PROFILE_HOME, "collection.anki2")
    MAIN_DECK = profile_data['MAIN_DECK']
    DECK = profile_data['DECK']
    MODEL = profile_data['MODEL']

    # Fields
    KANJI_1 = profile_data['kanji_field_1']
    KANJI_2 = profile_data['kanji_field_2']
    KANA = profile_data['kana_field']
    ENGLISH = profile_data['english_field']
    WORD_POS = profile_data['word_pos_field']
    AUDIO_FIELD = profile_data['audio']
    IMAGE_FIELD = profile_data['image']
    DEFINITION = profile_data['definition_field']
    EXAMPLE_SEN = profile_data['sentence_field']
    FREQ = profile_data['frequency']

    # Get the collection
    col = Collection(CPATH, log=True)
    os.chdir(CWD) # Change back to current working directory

#############
# MAIN CODE #
#############
def main():
    print('Running...')
    for cid in col.findNotes("deck:" + "\'" + DECK + "\'"):
        db_note = col.getNote(cid) # Get note from deck
        print(f"Current definition: {db_note.fields[DEFINITION]}")
        if db_note.fields[DEFINITION] == '': # Check the image field if empty
            entry = getWordInfo(db_note.fields[KANJI_1])
            if entry[4]:
                print(f'New def found for: {db_note.fields[KANJI_1]}')
                db_note.fields[DEFINITION] = entry[4]
                db_note.flush()
        col.save()


if __name__ == "__main__" and PROFILE:
    main()
