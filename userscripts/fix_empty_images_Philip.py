import sys, os

sys.path.append("../anki") 
from anki.storage import Collection
from image_downloader import imageDownload

PROFILE_HOME = "C:/Users/Philip Welin-Berger/AppData/Roaming/Anki2/User 1"
CPATH = os.path.join(PROFILE_HOME, "collection.anki2")

col = Collection(CPATH, log=True)

num_empty = 0

for cid in col.findNotes("deck:'3言:: 自分'"):
    #print('scanning')
    db_note = col.getNote(cid)
    print(f'Field is: {db_note.fields[1]}')
    #print(f'Field is: {db_note.fields[5]}')
    if len(db_note.fields) > 6:
        if db_note.fields[5] == '':
            num_empty += 1
            print('found empty')
            print(f"current field: {db_note.fields[5]}")
            (data, file_name) = imageDownload(db_note.fields[0], 100)
            if data == None:
                print(f"\nNo data returned for {db_note.fields[0]}")
            else:
                image_file = '<img src="' + file_name + '" />'
                col.media.writeData(file_name, data)
            db_note.fields[5] = image_file
            print(f"New field: {db_note.fields[5]}")
            db_note.flush()
    col.save()
print("Empty images: " + str(num_empty))

