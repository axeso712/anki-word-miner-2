#!/usr/bin/env python
import sys, os, json
from download_audio import audioDownload

sys.path.append(os.getcwd() + "\\anki")
from anki.storage import Collection

CWD = os.getcwd() # Get our working directory

################
# Load Profile #
################
if len(sys.argv) > 1:
    PROFILE = sys.argv[1]
else:
    PROFILE = None

try:
    with open('userscripts/profile_' + PROFILE + '.json','r',encoding='utf-8') as profile_file:
        profile_data = json.load(profile_file)
except OSError as e:
    print(f'ERROR: The profile for {PROFILE} was not found, is the name correct?')
    PROFILE = None
    profile_data = None
except TypeError as e:
    print(f"ERROR: You have to input profile as an argument")
    PROFILE = None
    profile_data = None

#################
#  Profile data #
#################
if profile_data:
    PROFILE_HOME = profile_data['PROFILE_HOME']
    CPATH = os.path.join(PROFILE_HOME, "collection.anki2")
    MAIN_DECK = profile_data['MAIN_DECK']
    DECK = profile_data['DECK']
    MODEL = profile_data['MODEL']

    # Fields
    KANJI_1 = profile_data['kanji_field_1']
    KANJI_2 = profile_data['kanji_field_2']
    KANA = profile_data['kana_field']
    ENGLISH = profile_data['english_field']
    WORD_POS = profile_data['word_pos_field']
    AUDIO_FIELD = profile_data['audio']
    IMAGE_FIELD = profile_data['image']
    DEFINITION = profile_data['definition_field']
    EXAMPLE_SEN = profile_data['sentence_field']
    FREQ = profile_data['frequency']

    # Get the collection
    col = Collection(CPATH, log=True)
    os.chdir(CWD) # Change back to current working directory

#############
# MAIN CODE #
#############

def main():
    num_empty_audio = 0
    for cid in col.findNotes("deck:" + "\'" + DECK + "\'"):
        db_note = col.getNote(cid) # Get note from deck
        if len(db_note.fields) > AUDIO_FIELD:
            if db_note.fields[AUDIO_FIELD] == '' and "no_sound" not in db_note.tags:
                (data, file_name) = audioDownload(db_note.fields[KANA], db_note.fields[KANJI_1])
                if data != None:
                    num_empty_audio += 1
                    col.media.writeData(file_name, data)
                    db_note.fields[AUDIO_FIELD] = u'[sound:{}]'.format(file_name)
                    print(f"New field: {db_note.fields[AUDIO_FIELD]}")
                else:
                    print(f"Added no_sound tag to {db_note.fields[KANJI_1]}")
                    db_note.tags.append("no_sound")
                db_note.flush()
            col.save()
    print("Fixed audio: " + str(num_empty_audio))


if __name__ == "__main__":
    main()

